open Graph
open Gfile
open FordFulkerson

let () =

  if Array.length Sys.argv <> 3 then
    begin
      Printf.printf "\nUsage: %s infile outfile\n\n%!" Sys.argv.(0) ;
      exit 0
    end ;

  let infile = Sys.argv.(1)
  and outfile = Sys.argv.(2)
  in

  (* Open file *)
  let graph = from_file infile in

  (* Rewrite the graph that has been read. *)
  let () = export outfile graph in

  (* Prints out the filename to be used in a Unix pipeline *)
  () ; Printf.printf "Successfuly outputed to %s\n%!" Sys.argv.(2)
