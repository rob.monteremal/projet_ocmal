open Graph
open Gfile
open FordFulkerson

let () =

  if Array.length Sys.argv <> 4 then
    begin
      Printf.printf "\nUsage: %s infile source well\n%!" Sys.argv.(0) ;
      exit 0
    end ;

  let infile = Sys.argv.(1)
  and source = Sys.argv.(2)
  and well = Sys.argv.(3)
  in

  (* Open file *)
  let graph = map (from_file infile) int_of_string in

  (* Converts a graph to a residual graph *)
  let residualGraph = convert graph in

  (* Runs FF on the residual graph *)
  let (_, maxFlow) = fordFulkerson residualGraph source well in

  (* Rewrite the graph that has been read. *)
  let () = Printf.printf "Max flow on this graph = %d\n%!" (maxFlow) in 

  ()
