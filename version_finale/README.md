2 files that can be compiled/run:
	- exportDot.ml : to convert a graph file (such as graph.txt) to a dot formated file
	- maxFlow.ml : applies the FordFulkerson algorithm to a graph file and outputs the max flow value

Useful commands (that you already know):
	- ocamlbuild file.ml file.byte : compiles a ml file
	- dot -Tpng dotFile.dot > image.png : converts a dot file to a png image
	- ./file.byte : runs a byte file (...that's just basic Unix stuff at this point, I'll stop)

Included in this package : a free graph file, called graph.txt!
