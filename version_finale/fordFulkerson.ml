(**** Récupération des autres Modules *****************************************)
open Graph
open Gfile

(**** Type ********************************************************************)
type pathway = Graph.id list
type 'a node = (id * 'a out_arcs)

(**** Constructeur ************************************************************)
let (empty_pathway:pathway) = []

(**** Fonctions booléennes ****************************************************)

(* isValid : 'a graph -> bool
 * Entrées : un graphe à valider
 * Sorties : true si le graphe peut être utilisé par l'algorithme de fordFulkerson, false sinon
 *)
let isValid aGraph = 
	let rec positiveLabel (_,valLabel)          = (valLabel >= 0)
	and positiveArcsList anArcList              = List.for_all positiveLabel anArcList
	and foldPositiveFct outBool idNode arcsList =	outBool && (positiveArcsList arcsList) in
	let isPositiveGraph                         = v_fold aGraph foldPositiveFct true 
	in
		isPositiveGraph

(**** Fonctions de conversion *************************************************)

(* convert : 'a graph -> 'a graph
 * Entrées : un graphe
 * Sorties : le graphe d'écart associé
 *)

let convert aGraph =
	(*Creates all nodes*)
	let createNode outGraph = v_fold aGraph (fun outGraph idNode _ -> (add_node outGraph idNode)) outGraph in

	(*Copy existing arc, create empty arc*)
	let copyCreateArc fromNode outGraph (toNode, arcValue) = match find_arc outGraph toNode fromNode with
		| None   -> add_arc (add_arc outGraph fromNode toNode arcValue) toNode fromNode 0
		| Some _ -> add_arc outGraph fromNode toNode arcValue
	in

	let run outputGraph =
		v_fold aGraph
					 (fun oGraph idFromNode arcList -> List.fold_left (copyCreateArc idFromNode)
																														oGraph
																														arcList
					 )
					 outputGraph
	in

	run (createNode empty_graph)


(**** Fonctions de recherche **************************************************)

(* getNode : 'a graph -> id -> 'a Node
 * Entrées : un graphe, une id
 * Sorties : le noeud correspondant à l'id
 * Erreur  : aucun noeud trouvé
 *)
let rec getNode aGraph idNode =	if node_exists aGraph idNode then (idNode, (out_arcs aGraph idNode)) else failwith "Don't exist"


(* findPathway : 'a graph -> id -> id -> pathway option
 * Entrées : un graphe
 * Sorties : un chemin entre les deux nodes
 * Erreur  : les nodes en entrée n'existent pas.
 *)

let findPathway aGraph aSource aWell =
	let rec depthFirstSearch someSource someGreyList =
		(* Récupération de la node *)		
		let (theNodeId, theArcList) = getNode aGraph someSource in
		(* Vérification du Résultat *)
		if (aWell = someSource) then (Some (List.rev (someSource::someGreyList))) else
		(* Sinon traitement *)
		subDepthFirstSearch theArcList (someSource::someGreyList)
	
	and subDepthFirstSearch anyArcList anyGreyList = match anyArcList with 
		| []                            -> None
		| (targetId ,disp)::restOfArcList -> 
			if ((List.exists (fun x -> (x = targetId)) anyGreyList)||(disp=0))
			then subDepthFirstSearch restOfArcList anyGreyList
			else 
				match (depthFirstSearch targetId anyGreyList ) with
					| None     -> subDepthFirstSearch restOfArcList anyGreyList
					| Some way -> Some way		
	in
		depthFirstSearch aSource empty_pathway
		

(**** Fonctions de traitement *************************************************)

(* minCapacity : 'a Pathway
 * Entrées : un chemin
 * Sortie  : la valeur de la plus petite capacité sur le chemin
 *)
let minCapacity aGraph (aPathway:pathway) = 
	let rec subMinCapacity somePathway optionCapacity = match (somePathway, optionCapacity) with
		| ([]                                        , None              ) -> failwith "Empty Path"
		| ([]                                        , Some minCapa      ) -> minCapa
		| (lastNode::[]                              , None              ) -> 0
		| (lastNode::[]                              , Some minCapa      ) -> minCapa
		| (presentNodeId::(nextNodeId::restOfPathway), None              ) -> 
				begin
					match (find_arc aGraph presentNodeId nextNodeId) with 
						| None 					    -> failwith "Pathway does not match with Graph"
						| Some valArc       -> subMinCapacity (nextNodeId::restOfPathway) (Some valArc)
				end
		| (presentNodeId::(nextNodeId::restOfPathway), Some actualMinCapa) -> 
				begin
					match (find_arc aGraph presentNodeId nextNodeId) with 
						| None						  -> failwith "Pathway does not match with Graph"
						| Some valArc       ->	if actualMinCapa < valArc then subMinCapacity (nextNodeId::restOfPathway) (Some actualMinCapa) else subMinCapacity (nextNodeId::restOfPathway) (Some valArc)
				end
	in
		subMinCapacity aPathway None

(**** Fonctions Principales ***************************************************)

(* fordFulkerson : 'a graph -> 'a -> 'a -> ('a graph * 'a )
 * Entrées : un graphe, une source, un puits
 * Sortie  : (un graphe d'écart final, le flot max)
 *)
let fordFulkerson aGraph aSource aWell = 
	let rec decResGraph rGraph aPath aValue = match aPath with
		| []                               -> rGraph
		| [_]                              -> rGraph
		| (fromNode::(toNode::restOfPath)) -> 
				begin 
					match (find_arc rGraph fromNode toNode, find_arc rGraph toNode fromNode) with
						| (None       , _          ) -> failwith "Unable to find a requested arc"
						| (_          , None       ) -> failwith "Unable to find a requested arc"
						| (Some dirVal, Some revVal) -> decResGraph (add_arc (add_arc rGraph fromNode toNode (dirVal-aValue)) toNode fromNode (revVal+aValue)) (toNode::restOfPath) aValue
				end
	in
	let rec iterFF residualGraph currentFlow = match (findPathway residualGraph aSource aWell) with
		| None      -> (residualGraph, currentFlow)
		| Some path -> iterFF (decResGraph residualGraph path (minCapacity residualGraph path)) (currentFlow + (minCapacity residualGraph path))
	in	
	begin
		match isValid aGraph with
			| false -> failwith "Invalid input graph"
			| true  -> iterFF (convert aGraph) 0
	end

(**** Fonction de test ********************************************************)

let printPathway aWay = 
	let rec subPrintPathway someWay = match someWay with
	| []         -> ""	
	| elmt::[]   -> (elmt)
	| elmt::rest -> (elmt) ^ "->" ^ (subPrintPathway rest)
	in 
		Printf.printf "%s\n%!" (subPrintPathway aWay)

