(* Récupération des autres Modules *)
open Graph
open Gfile

(* Valide que l'algorithme de Ford-Fulkerson peut-être appliquer au graphe*)
val isValid : int graph -> bool

(* Applique l'algorithme de Ford-Fulkerson avec passage en paramètre la source et le puit, retourne un tuple du graphe d'écart final et le flot max*)
val fordFulkerson : int graph -> id -> id -> (int graph * int)

(* Transforme un graphe en graphe d'écart *)
val convert : int graph -> int graph
