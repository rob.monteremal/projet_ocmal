open Graph
open Gfile
open FordFulkerson

let () =

  if Array.length Sys.argv <> 5 then
    begin
      Printf.printf "\nUsage: %s infile outfile\n\n%!" Sys.argv.(0) ;
      exit 0
    end ;

  let infile = Sys.argv.(1)
  and outfile = Sys.argv.(2)
  and source = Sys.argv.(3)
  and well = Sys.argv.(4)
  in

  (* Open file *)
  let graph = from_file infile in

	(* Map the labels *)
	let mapGraph = Graph.map graph int_of_string in

  (* Finds and prints a pathway in a graph *)
  let pathOption = findPathway mapGraph source well in

  (* Prints out the pathway *)
  match pathOption with 
		| None      -> Printf.printf "Lol, no way.\n%!"
		| Some path -> printPathway path ; Printf.printf "Minimum Capacity on this path : %d\n%!" (minCapacity mapGraph path) ; Printf.printf "FF can be applied to this graph : %B\n%!" (isValid mapGraph)
