open Graph
open Gfile
open FordFulkerson

let () =

  if Array.length Sys.argv <> 3 then
    begin
      Printf.printf "\nUsage: %s infile outfile\n\n%!" Sys.argv.(0) ;
      exit 0
    end ;

  let infile = Sys.argv.(1)
  and outfile = Sys.argv.(2)
  in

  (* Open file *)
  let graph = map (from_file infile) int_of_string in

  (* Converts a graph to a residual graph *)
  let someGraph = convert graph in

  (* Rewrite the graph that has been read. *)
  let () = export outfile (map someGraph string_of_int) in 

  ()
